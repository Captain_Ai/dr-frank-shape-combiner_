﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;

namespace Dr_Frank
{
    public partial class Form1 : Form
    {
        // http://wiki.secondlife.com/wiki/Appearance_Editor_and_affected_bones
        int[] tab_body  = { 33, 34, 637, 11001 };
        int[] tab_head  = { 682, 647, 193, 646, 773, 662, 629, 1, 18, 10, 14 };
        int[] tab_eyes  = { 690, 24, 196, 650, 880, 769, 21, 23, 765, 518, 664 };
        int[] tab_ears  = { 35, 15, 22, 796 };
        int[] tab_nose  = { 2, 517, 4, 759, 20, 11, 758, 27, 19, 6, 656 };
        int[] tab_mouth = { 155, 653, 505, 799, 506, 659, 764, 25, 663 };
        int[] tab_chin  = { 7, 17, 185, 760, 665, 12, 5, 13, 8 };
        int[] tab_torso = { 649, 678, 683, 756, 36, 105, 507, 684, 685, 693, 675, 38, 676, 157 };
        int[] tab_legs  = { 652, 692, 37, 842, 795, 879, 753, 841, 515 };

        string saveDir;
        

        public Form1()
        {
            InitializeComponent();
        }

        private void ReplaceIds(int[] ids, XmlDocument source, XmlDocument target)
        {
            foreach(var id in ids)
            {
                DeleteIdIfExists(id, target);
                AddIdNodeIfExists(id, source, target);
            }
        }

        private void DeleteIdIfExists(int id, XmlDocument document)
        {
            XmlNode node = document.SelectSingleNode("linden_genepool/archetype/param[@id='" + id + "']");
            if (node != null)
            {
                node.ParentNode.RemoveChild(node);
            }
        }

        private void AddIdNodeIfExists(int id, XmlDocument source, XmlDocument target)
        {
            XmlNode node = source.SelectSingleNode("linden_genepool/archetype/param[@id='" + id + "']");
            if (node != null)
            {
                XmlNode importedNode = target.ImportNode(node, true);
                XmlNode target_archetype = target.SelectSingleNode("linden_genepool/archetype");
                target_archetype.PrependChild(importedNode);
            }
        }

        private string browse4file(string initialDir = "")
        {
            if (initialDir != "" && Directory.Exists(new FileInfo(initialDir).DirectoryName))
            {
                initialDir = new FileInfo(initialDir).DirectoryName;
            } else
            {
                initialDir = "";
            }
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Title = "Select Appearance XML File";
            ofd.Filter = "Appearance XML Files (.xml)|*.xml";
            if (initialDir != "") ofd.InitialDirectory = initialDir;
            DialogResult dr = ofd.ShowDialog(this);
            if (dr == DialogResult.OK)
            {
                saveDir = new FileInfo(ofd.FileName).DirectoryName;
                return ofd.FileName;
            } else
            {
                return "";
            }
        }

        private void buttonBrowseHead_Click(object sender, EventArgs e)
        {
            string file = browse4file(textBoxHeadShape.Text);
            if (file != "")
            {
                textBoxHeadShape.Text = file;
            }
        }

        private void buttonBrowseBody_Click(object sender, EventArgs e)
        {
            string file = browse4file(textBoxBodyShape.Text);
            if (file != "")
            {
                textBoxBodyShape.Text = file;
            }
        }

        private void buttonCombine_Click(object sender, EventArgs e)
        {
            if (textBoxBodyShape.Text == "" || textBoxHeadShape.Text == "")
            {
                MessageBox.Show("Must set shape files first");
                return;
            }
            try
            {
                XmlDocument head = new XmlDocument();
                head.Load(textBoxHeadShape.Text);
                XmlDocument body = new XmlDocument();
                body.Load(textBoxBodyShape.Text);


                //int[] tab_body = { 33, 34, 637, 11001 };
                //int[] tab_head = { 682, 647, 193, 646, 773, 662, 629, 1, 18, 10, 14 };
                //int[] tab_eyes = { 690, 24, 196, 650, 880, 769, 21, 23, 765, 518, 664 };
                //int[] tab_ears = { 35, 15, 22, 796 };
                //int[] tab_nose = { 2, 517, 4, 759, 20, 11, 758, 27, 19, 6, 656 };
                //int[] tab_mouth = { 155, 653, 505, 799, 506, 659, 764, 25, 663 };
                //int[] tab_chin = { 7, 17, 185, 760, 665, 12, 5, 13, 8 };
                //int[] tab_torso = { 649, 678, 683, 756, 36, 105, 507, 684, 685, 693, 675, 38, 676, 157 };
                //int[] tab_legs = { 652, 692, 37, 842, 795, 879, 753, 841, 515 };

                ReplaceIds(tab_head, head, body);
                ReplaceIds(tab_eyes, head, body);
                ReplaceIds(tab_ears, head, body);
                ReplaceIds(tab_nose, head, body);
                ReplaceIds(tab_mouth, head, body);
                ReplaceIds(tab_chin, head, body);

                SaveFileDialog sfd = new SaveFileDialog();
                sfd.Filter = "Appearance XML (.xml)|*.xml";
                sfd.Title = "Save Shape";
                sfd.FileName = "Combined.xml";
                sfd.InitialDirectory = saveDir;
                if (sfd.ShowDialog() == DialogResult.OK)
                {
                    body.Save(sfd.FileName);
                }

            } catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "An error occurred", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
        }
    }
}
