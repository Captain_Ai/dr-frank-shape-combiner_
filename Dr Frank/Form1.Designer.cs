﻿namespace Dr_Frank
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBoxHeadShape = new System.Windows.Forms.TextBox();
            this.buttonBrowseHead = new System.Windows.Forms.Button();
            this.buttonBrowseBody = new System.Windows.Forms.Button();
            this.textBoxBodyShape = new System.Windows.Forms.TextBox();
            this.buttonCombine = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // textBoxHeadShape
            // 
            this.textBoxHeadShape.Location = new System.Drawing.Point(11, 39);
            this.textBoxHeadShape.Name = "textBoxHeadShape";
            this.textBoxHeadShape.Size = new System.Drawing.Size(373, 20);
            this.textBoxHeadShape.TabIndex = 0;
            // 
            // buttonBrowseHead
            // 
            this.buttonBrowseHead.Location = new System.Drawing.Point(390, 39);
            this.buttonBrowseHead.Name = "buttonBrowseHead";
            this.buttonBrowseHead.Size = new System.Drawing.Size(75, 23);
            this.buttonBrowseHead.TabIndex = 1;
            this.buttonBrowseHead.Text = "Browse...";
            this.buttonBrowseHead.UseVisualStyleBackColor = true;
            this.buttonBrowseHead.Click += new System.EventHandler(this.buttonBrowseHead_Click);
            // 
            // buttonBrowseBody
            // 
            this.buttonBrowseBody.Location = new System.Drawing.Point(390, 89);
            this.buttonBrowseBody.Name = "buttonBrowseBody";
            this.buttonBrowseBody.Size = new System.Drawing.Size(75, 23);
            this.buttonBrowseBody.TabIndex = 3;
            this.buttonBrowseBody.Text = "Browse...";
            this.buttonBrowseBody.UseVisualStyleBackColor = true;
            this.buttonBrowseBody.Click += new System.EventHandler(this.buttonBrowseBody_Click);
            // 
            // textBoxBodyShape
            // 
            this.textBoxBodyShape.Location = new System.Drawing.Point(11, 92);
            this.textBoxBodyShape.Name = "textBoxBodyShape";
            this.textBoxBodyShape.Size = new System.Drawing.Size(373, 20);
            this.textBoxBodyShape.TabIndex = 2;
            // 
            // buttonCombine
            // 
            this.buttonCombine.Location = new System.Drawing.Point(390, 121);
            this.buttonCombine.Name = "buttonCombine";
            this.buttonCombine.Size = new System.Drawing.Size(75, 40);
            this.buttonCombine.TabIndex = 4;
            this.buttonCombine.Text = "Combine...";
            this.buttonCombine.UseVisualStyleBackColor = true;
            this.buttonCombine.Click += new System.EventHandler(this.buttonCombine_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.Control;
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 180);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(477, 23);
            this.panel1.TabIndex = 5;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(67, 13);
            this.label1.TabIndex = 6;
            this.label1.Text = "Head Shape";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 76);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(65, 13);
            this.label2.TabIndex = 7;
            this.label2.Text = "Body Shape";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(8, 5);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(99, 13);
            this.label3.TabIndex = 0;
            this.label3.Text = "By Ai (extrude.ragu)";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(405, 5);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(60, 13);
            this.label4.TabIndex = 1;
            this.label4.Text = "Version 1.0";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ClientSize = new System.Drawing.Size(477, 203);
            this.Controls.Add(this.buttonCombine);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.buttonBrowseBody);
            this.Controls.Add(this.textBoxBodyShape);
            this.Controls.Add(this.buttonBrowseHead);
            this.Controls.Add(this.textBoxHeadShape);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Form1";
            this.ShowIcon = false;
            this.Text = "Dr Frank - Head and Body Shape Combiner";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBoxHeadShape;
        private System.Windows.Forms.Button buttonBrowseHead;
        private System.Windows.Forms.Button buttonBrowseBody;
        private System.Windows.Forms.TextBox textBoxBodyShape;
        private System.Windows.Forms.Button buttonCombine;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
    }
}

